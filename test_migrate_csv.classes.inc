<?php
/**
 *
 */
class TestCSVMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Import Nodes');
    $columns = array(
      0 => array('id_csv', t('ID')),
      1 => array('title_csv', t('Title')),
      2 => array('body_csv', t('Body')),
    );

    $options = array(
      'header_rows' => 1,
      'delimiter' => ',',
    );

    $this->source = new MigrateSourceCSV(drupal_get_path('module', 'test_migrate_csv') . '/test.csv', $columns, $options);
    $this->destination = new MigrateDestinationNode('page');

    $source_key_schema = array(
      'id_csv' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    );

    $this->map = new MigrateSQLMap($this->machineName, $source_key_schema, MigrateDestinationNode::getKeySchema());

    $this->addFieldMapping('title', 'title_csv');
    $this->addFieldMapping('body', 'body_csv');
  }
}
